# This file is a template, and might need editing before it works on your project.
FROM ruby:2.7.2

# Edit with nodejs, mysql-client, postgresql-client, sqlite3, etc. for your needs.
# Or delete entirely if not needed.
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        nodejs \
        npm \
        postgresql-client \
    && rm -rf /var/lib/apt/lists/*
RUN npm install -g yarn

# throw errors if Gemfile has been modified since Gemfile.lock
RUN bundle config --global frozen 1

WORKDIR /usr/src/app

COPY Gemfile Gemfile.lock /usr/src/app/
RUN bundle install -j $(nproc)

COPY . /usr/src/app

# For Rails
RUN yarn install --check-files
RUN RAILS_ENV=production bundle exec rake assets:precompile

EXPOSE 5000
CMD ["bundle", "exec", "rails", "server", "-b", "0.0.0.0", "-p", "5000"]


